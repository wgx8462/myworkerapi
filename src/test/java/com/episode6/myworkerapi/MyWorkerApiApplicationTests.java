package com.episode6.myworkerapi;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SpringBootTest
class MyWorkerApiApplicationTests {

    @Autowired
    private DataSourceTransactionManagerAutoConfiguration dataSourceTransactionManagerAutoConfiguration;

    @Test
    void initTest() {
        String today = "2024년 5월 2일";
        today = today.replace("년 ", "-");

        System.out.println(today);
    }

    @Test
    void gpsTest() {
        double lat1 = 37.321910811286266;
        double lon1 = 126.83084007043729;
        double lat2 = 37.31934283132678;
        double lon2 = 126.81864735365073;

        double theta = lon1 - lon2;
        double deg1rad = lat1 * Math.PI / 180.0;
        double deg2rad = lat2 * Math.PI / 180.0;
        double deg3rad = theta * Math.PI / 180.0;

        double dist = Math.sin(deg1rad) * Math.sin(deg2rad) + Math.cos(deg1rad) * Math.cos(deg2rad) * Math.cos(deg3rad);

        dist = Math.acos(dist);
        double rad2deg = dist * 180 / Math.PI;
        dist = rad2deg;
        dist = dist * 60 * 1.1515;
        dist = dist * 1609.344;

        System.out.println(dist);
    }

    @Test
    void contextLoads() {
        String[] today = {"월", "화", "수", "목", "금"};
        String[] time = {"09:00", "10:00", "11:00", "12:00", "13:00"};
        String[] end = {"18:00", "19:00", "20:00", "21:00", "22:00", "23:00"};
        String as = today[0] + "요일 " + time[0] + " ~ " + end[0];

        System.out.println(as);
    }

    @Test
    void intTexts() {
        int[] texts = {2010, 2934, 1923};

        int[] sortedArray = IntStream.of(texts).boxed()
                .sorted(Comparator.reverseOrder())
                .mapToInt(Integer::intValue)
                .toArray();

        System.out.println("-------------------");
        System.out.println("sortedArray = " + Arrays.toString(sortedArray));
        System.out.println("-------------------");
    }

    @Test
    void todayWeek() {
        int toWeek = LocalDate.now().getDayOfWeek().getValue();
        String today = switch (toWeek) {
            case 1 -> "월";
            case 2 -> "화";
            case 3 -> "수";
            case 4 -> "목";
            case 5 -> "금";
            case 6 -> "토";
            case 7 -> "일";
            default -> "";
        };

        String[] weekText = new String[]{"월", "화", "수", "목", "금", "토", "일"};
        String todayText = weekText[toWeek - 1];
        System.out.println(todayText);
    }

    @Test
    void stet() {
        // 오늘과 같은 요일에 배열 시간을 사용하고 싶다. 어떻게 해야 할까?
        String[] today = {"MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY"};
        String[] time = {"09:00", "10:00", "11:00", "12:00", "13:00"};

        // 스트림도 결국 고급진 for문이다.

        int as = Arrays.asList(today).indexOf(LocalDate.now().getDayOfWeek().name());

        System.out.println(as);
        System.out.println(time[as]);

        String targetDay = "THURSDAY";

        Optional<String> result = Arrays.stream(today)
                .filter(day -> day.equals(targetDay))
                .findFirst();

        if (result.isPresent()) {
            int index = Arrays.asList(today).indexOf(result.get());
            if (index != -1) {
                System.out.println("Requested time for " + targetDay + ": " + time[index]);
            } else {
                System.out.println("Requested day not found.");
            }
        } else {
            System.out.println("Requested day not found.");
        }



        IntStream
                .range(0, today.length)
                .mapToObj(index -> String.format("%d -> %s", index, today[index]))
                .forEach(System.out::println);



        AtomicInteger index = new AtomicInteger(0);
        Arrays.stream(today)
                .forEach(name -> {
                    int currentIndex = index.getAndIncrement();
                    System.out.println("인덱스: " + currentIndex + ", 이름: " + name);
                });


        System.out.println("--------------------");
        System.out.println(Arrays.toString(today));
        System.out.println(LocalDate.now().getDayOfWeek().name());

        System.out.println("--------------------");
    }

    @Test
    void stringTexts() {
        String[] names = {"홍길동", "박기기", "김길동", "김오민", "오로나", "미나미", "무지개", "김길하", "퍼스트", "이휘재"};

        String[] strStream = Arrays.stream(names)
                .filter((name) -> name.startsWith("김"))
                .sorted()
                .toArray(String[]::new);

        System.out.println("-------------------");
        System.out.println("strStream = " + Arrays.toString(strStream));
        System.out.println("-------------------");


        Stream<String> nameStream = Stream.of(names)
                .filter((n) -> n.startsWith("김"))
                .sorted();

        System.out.println("-------------------");
        System.out.println(nameStream.toList());
        System.out.println("-------------------");
    }

    @Test
    void dayAll() {
        LocalDateTime startTime = LocalDateTime.of(2024, 4, 1, 0, 0);
        LocalDateTime endTime = startTime.plusMonths(1).minusDays(1);

        System.out.println(startTime);
        System.out.println(endTime);
    }

    private long getDayPay(double timePerPay, int totalWorkTime) {
        int fixTime = 8;
        double basePay = totalWorkTime <= fixTime ? totalWorkTime * timePerPay : fixTime * totalWorkTime;
        double addPay = totalWorkTime > fixTime ? (totalWorkTime - fixTime) * (timePerPay * 1.5) : 0D;

        return Math.round(basePay + addPay);
    }
}

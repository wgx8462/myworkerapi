package com.episode6.myworkerapi.exception;

public class CReportCommentOverlapException extends RuntimeException{

    public CReportCommentOverlapException(String msg, Throwable t) {
        super(msg,t);
    }
    public CReportCommentOverlapException(String msg) {
        super(msg);
    }
    public CReportCommentOverlapException() {
        super();
    }
}


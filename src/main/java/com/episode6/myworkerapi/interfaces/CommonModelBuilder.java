package com.episode6.myworkerapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

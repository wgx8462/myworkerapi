package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter

public enum Position {
    MANAGER("매니저"),
    TEAM_LEADER("팀리더"),
    SENIOR_STAFF("월급제 알바생"),
    STAFF("시급제 알바생"),
    TEAM_TOP("부사장"),
    YOU_FIRED("퇴사");

    private final String name;

    public static Position findByName(String name) {
        return Arrays.stream(values())
                .filter(position -> position.getName().equals(name))
                .findFirst()
                .orElse(null);
    }
}

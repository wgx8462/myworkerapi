package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberType {
    ROLE_GENERAL("일반회원"),
    ROLE_BOSS("사업자"),
    ROLE_MANAGEMENT("관리자");

    private final String name;
}

package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    ,CHANGE(0,"수정 되었습니다.")
    ,FAILURE(-1,"실패하였습니다")  // 실패에 대한 / 왜 실패했는지 메시지 Exception
    ,NEW_ID(0,"아이디 생성가능")
    ,NOT_ID(-1, "중복 아이디가 있습니다.")
    ,ANSWER(0, "삭제할 수 없습니다.")
    ,DEL_ANSWER(0, "삭제되었습니다.")

    ,ACCESS_DENIED(-2, "접근 권한이 없습니다.")
    ,ENTRY_POINT(-3, "로그인을 해주세요.")

    ,MEMBER_USERNAME(-1000, "아이디가 없습니다.")
    ,PASSWORD(-1001, "비밀번호 확인 실패 다시 한번 확인해주세요.")

    ,BUSINESS_OVERLAP(-2001,"중복된 사업장 등록 요청입니다.")
    ,REPORT_BOARD_OVERLAP(-15001, "중복된 게시글 신고 요청입니다.")
    ,REPORT_COMMENT_OVERLAP(-16001, "중복된 댓글 신고 요청입니다.")
    ,DISTANCE_OUT(-17001, "근무지에서 너무 멉니다.")
    ,AGREE(0, "요청 수락되었습니다.")
    ,SIGN_UP(0, " 요청 완료되었습니다.")
    ;

    private final Integer code;
    private final String msg;
}

package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Category {
    INTEGRATION("통합"),
    BOSS("사업자"),
    JOB("일반회원");


    private final String name;
}

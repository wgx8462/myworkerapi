package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.MemberAsk;
import com.episode6.myworkerapi.model.askanswer.AskAnswerItem;
import com.episode6.myworkerapi.model.askanswer.AskAnswerRequest;
import com.episode6.myworkerapi.model.askanswer.AskAnswerResponse;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ask-answer")
public class AskAnswerController {
    private final ProfileService profileService;
    private final MemberAskService memberAskService;
    private final AskAnswerService askAnswerService;

    @PostMapping(value = "/member-ask/{memberAskId}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "문의 내용 답변 등록")
    public CommonResult setAskAnswer(@PathVariable long memberAskId, @RequestPart(required = false) MultipartFile multipartFile, AskAnswerRequest request) throws IOException {
        Member member = profileService.getDate();
        MemberAsk memberAsk = memberAskService.getMemberAskData(memberAskId);
        askAnswerService.setAskAnswer(member, memberAsk, request,multipartFile);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의 답변 리스트 관리자용")
    public ListResult<AskAnswerItem> getAskAnswers(@PathVariable int pageNum) {
        return ResponseService.getListResult(askAnswerService.getAskAnswers(pageNum),true);
    }

    @GetMapping("/detail/ask-answer-id/{askAnswerId}")
    @Operation(summary = "문의 답변 상세보기")
    public SingleResult<AskAnswerResponse> getAskAnswer(@PathVariable long askAnswerId) {
        return ResponseService.getSingleResult(askAnswerService.getAskAnswer(askAnswerId));
    }

    @GetMapping("/detail/member-ask-id/{memberAskId}")
    @Operation(summary = "문의 시퀀스로 답변 상세보기")
    public SingleResult<AskAnswerResponse> getAskAnswerMemberAsk(@PathVariable long memberAskId) {
        return ResponseService.getSingleResult(askAnswerService.getAskAnswerMemberAsk(memberAskId));
    }

    @PutMapping("/{id}")
    @Operation(summary = "문의 답변 수정하기")
    public CommonResult putAskAnswer(@PathVariable long id, @RequestBody @Valid AskAnswerRequest request) {
        askAnswerService.putAskAnswer(id, request);
        return ResponseService.getSuccessResult();
    }
}

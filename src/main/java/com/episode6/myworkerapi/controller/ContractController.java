package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.contract.ContractRequest;
import com.episode6.myworkerapi.model.contract.ContractResponse;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/contract")
public class ContractController {
    private final ContractService contractService;
    private final BusinessMemberService businessMemberService;
    private final ProfileService profileService;
    private final BusinessService businessService;

    @PostMapping("join/business-memberId/{businessMemberId}")
    @Operation(summary = "계약 등록")
    public CommonResult setContract(@RequestBody ContractRequest contractRequest, @PathVariable long businessMemberId) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        contractService.setContract(businessMember,contractRequest);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("detail/contract-id/{contractId}")
    @Operation(summary = "계약서 자세히 보기")
    public SingleResult<ContractResponse> getContract(@PathVariable long contractId) {
        return ResponseService.getSingleResult(contractService.getContract(contractId));
    }

    @GetMapping("detail/worker/contract-page/{pageNum}/business-id/{businessId}")
    @Operation(summary = "알바생용 계약서 페이징")
    public ListResult<ContractResponse> getWorkerContract(@PathVariable int pageNum, @PathVariable long businessId) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(contractService.getWorkerContract(pageNum,business,member),true);
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "계약서 페이징 최신순 관리자용")
    public ListResult<ContractResponse> getContracts(@PathVariable int pageNum) {
        return ResponseService.getListResult(contractService.getContracts(pageNum),true);
    }

    @GetMapping("all/boss/{pageNum}")
    @Operation(summary = " 사장님 사업장 계약 페이징")
    public ListResult<ContractResponse> getOwnerContractPage( @PathVariable int pageNum) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        return ResponseService.getListResult(contractService.getOwnerContractPage(pageNum, business), true);
    }
}

package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.businessmember.*;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/business-member")
public class BusinessMemberController {
    private final ProfileService profileService;
    private final BusinessService businessService;
    private final BusinessMemberService businessMemberService;

    @PostMapping("/join")
    @Operation(summary = "사업장 직원 등록")
    public CommonResult setBusinessMember( @RequestBody @Valid BusinessMemberRequest request) {
        Member member = profileService.getDate();
        Business business = businessService.getBusinessData(member);
        businessMemberService.setBusinessMember(business, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "사업장 직원 리스트로 보기 최신순 관리자용")
    public ListResult<BusinessMemberItem> getBusinessMemberAll(@PathVariable int pageNum) {
        return ResponseService.getListResult(businessMemberService.getBusinessMemberAll(pageNum), true);
    }

    @GetMapping("/all/member-list")
    @Operation(summary = "회원이 현재 등록되어있는 사업장 리스트로보기")
    public ListResult<BusinessMemberMyList> getMemberMyList() {
        Member member = profileService.getDate();
        return ResponseService.getListResult(businessMemberService.getMemberMyList(member), true);
    }

    @GetMapping("/all/my-business/{pageNum}")
    @Operation(summary = "내 사업장 재직자부터 퇴직자까지 리스트로 보기 사업자용")
    public ListResult<BusinessMemberItem> getBusinessOnlyMember(@PathVariable int pageNum) {
        Member member = profileService.getDate();
        return ResponseService.getListResult(businessMemberService.getBusinessOnlyMember(member, pageNum), true);
    }

    @GetMapping("/all/is-work/{pageNum}")
    @Operation(summary = "내 사업장 재직자만 리스트로 보기 사업자용")
    public ListResult<BusinessMemberItem> getBusinessOnlyMemberIsWork(@PathVariable int pageNum) {
        Member member = profileService.getDate();
        return ResponseService.getListResult(businessMemberService.getBusinessOnlyMemberIsWork(member, pageNum), true);
    }

    @GetMapping("/detail/business-member-id/{businessMemberId}")
    @Operation(summary = "사업장 알바생 상세보기")
    public SingleResult<BusinessMemberResponse> getBusinessMember(@PathVariable long businessMemberId) {
        return ResponseService.getSingleResult(businessMemberService.getBusinessMember(businessMemberId));
    }

    @PutMapping("/change/work/business-member-id/{businessMemberId}")
    @Operation(summary = "알바생 퇴직 재직 수정")
    public CommonResult putMemberChangeWork(@PathVariable long businessMemberId) {
        businessMemberService.putMemberChangeWork(businessMemberId);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/schedule/business-member-id/{businessMemberId}")
    @Operation(summary = "알바생 근무일정 등록 및 수정")
    public CommonResult putMemberSchedule(@PathVariable long businessMemberId, @RequestBody BusinessMemberScheduleRequest request) {
        businessMemberService.putMemberSchedule(businessMemberId, request);
        return ResponseService.getSuccessResult();
    }
}

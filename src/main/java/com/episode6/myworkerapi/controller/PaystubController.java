package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.paystub.PaystubResponse;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.PaystubService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/paystub")
public class PaystubController {
    private final PaystubService paystubService;
    private final MemberService memberService;

    @PostMapping("/month-pay/member-id/{memberId}")
    @Operation(summary = "알바생 시급으로 월급 계산")
    public SingleResult<PaystubResponse> getMonthlyPay(@PathVariable long memberId, int year, int month) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getSingleResult(paystubService.getMonthlyPay(member, year, month));
    }
}

package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.model.request.RefuseReasonRequest;
import com.episode6.myworkerapi.model.request.RequestItem;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.RequestCreateService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/request")
public class RequestController {
    private final RequestCreateService requestCreateService;
    private final BusinessService businessService;

    @PostMapping("join/business-id/{businessId}")
    @Operation(summary = "사업장 등록 요청 중복불가")
    public CommonResult setRequest(@PathVariable long businessId)throws Exception {
        Business business = businessService.getBusinessData(businessId);
        requestCreateService.setRequest(business);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("Agree/request-id/{requestId}")
    @Operation(summary = "요청수락 , 사업장 승인여부 true 변경, 비지니스멤버에 사장님 등록됨")
    public CommonResult putRequestAgree(@PathVariable long requestId) {
        requestCreateService.putRequestAgree(requestId);
        return ResponseService.getSuccessResult();
    }

    @PostMapping("RefuseReason/request-id/{requestId}")
    @Operation(summary = "요청 거절, 거절 사유 입력")
    private CommonResult setRefuseReason(@RequestBody RefuseReasonRequest refuseReasonRequest, @PathVariable long requestId) {
        requestCreateService.setRefuseReason(requestId,refuseReasonRequest);
        return ResponseService.getSuccessResult();

    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "등록요청 최신순 페이징")
    public ListResult<RequestItem> getRequestPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(requestCreateService.getRequestPage(pageNum),true);
    }
}

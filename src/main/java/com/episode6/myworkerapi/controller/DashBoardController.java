package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.dashboard.DashboardResponse;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.DashBoardTwoService;
import com.episode6.myworkerapi.service.ProfileService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/dashboard")
public class DashBoardController {
    private final DashBoardTwoService dashBoardTwoService;
    private final ProfileService profileService;

    @GetMapping("full-view/")
    @Operation(summary = "대시보드")
    public SingleResult<DashboardResponse> getDashboard() {
        Member member = profileService.getDate();
        return ResponseService.getSingleResult(dashBoardTwoService.getDashboard(member));
    }
}

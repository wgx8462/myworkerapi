package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMemberWeekList {
    private String workingHour;

    private BusinessMemberWeekList(Builder builder) {
        this.workingHour = builder.workingHour;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberWeekList> {
        private final String workingHour;

        public Builder(BusinessMember businessMember) {
            String[] week = businessMember.getWeekSchedule().split("@");
            String[] start = businessMember.getTimeScheduleStart().split("@");
            String[] end = businessMember.getTimeScheduleEnd().split("@");

            StringBuilder resultFixMemo = new StringBuilder();
            for (int i = 0; i < week.length; i++) {
                String as = week[i] + "요일 " + start[i] + " ~ " + end[i];
                if (i > 0) {
                    resultFixMemo.append(",");
                }
                resultFixMemo.append(as);
            }
            this.workingHour = resultFixMemo.toString();
        }
        @Override
        public BusinessMemberWeekList build() {
            return new BusinessMemberWeekList(this);
        }
    }
}

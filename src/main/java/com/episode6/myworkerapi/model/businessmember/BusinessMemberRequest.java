package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.enums.Position;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessMemberRequest {
    @NotNull
    @Schema(description = "등록할 알바생 폰번호")
    private String phoneNumber;
    @NotNull
    @Schema(description = "직책")
    private Position position;
    @Schema(description = "메모", nullable = true)
    private String etc;
}

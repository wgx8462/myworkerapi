package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Arrays;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMemberStartDayItem {
    private String businessMemberName;
    private String businessMemberPosition;
    private String startWork;
    private String endWork;

    private BusinessMemberStartDayItem(Builder builder) {
        this.businessMemberName = builder.businessMemberName;
        this.businessMemberPosition = builder.businessMemberPosition;
        this.startWork = builder.startWork;
        this.endWork = builder.endWork;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberStartDayItem> {
        private final String businessMemberName;
        private final String businessMemberPosition;
        private final String startWork;
        private final String endWork;

        public Builder(BusinessMember businessMember, String todayText) {
            String weekSchedule = businessMember.getWeekSchedule();
            String startTime = businessMember.getTimeScheduleStart();
            String endTime = businessMember.getTimeScheduleEnd();
            String[] today = weekSchedule.split("@");
            String[] startTimes = startTime.split("@");
            String[] endTimes = endTime.split("@");
            int index = Arrays.asList(today).indexOf(todayText);
            String startDayTime = startTimes[index];
            String endDayTime = endTimes[index];

            this.businessMemberName = businessMember.getMember().getName();
            this.businessMemberPosition = businessMember.getPosition().getName();
            this.startWork = startDayTime;
            this.endWork = endDayTime;
        }

        @Override
        public BusinessMemberStartDayItem build() {
            return new BusinessMemberStartDayItem(this);
        }
    }
}

package com.episode6.myworkerapi.model.product;

import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductResponse {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "멤버 시퀀스")
    private Long memberId;
    @Schema(description = "멤버 이름")
    private String memberName;
    @Schema(description = "등록 날짜")
    private String dateProduct;
    @Schema(description = "상품 이름")
    private String productName;
    @Schema(description = "기준이 될 최소 개수")
    private Short minQuantity;
    @Schema(description = "현재 개수")
    private Short nowQuantity;

    private ProductResponse(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.dateProduct = builder.dateProduct;
        this.productName = builder.productName;
        this.minQuantity = builder.minQuantity;
        this.nowQuantity = builder.nowQuantity;
    }

    public static class Builder implements CommonModelBuilder<ProductResponse> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final String dateProduct;
        private final String productName;
        private final Short minQuantity;
        private final Short nowQuantity;

        public Builder(Product product) {
            this.id = product.getId();
            this.memberId = product.getMember().getId();
            this.memberName = product.getMember().getName();
            this.dateProduct = product.getDateProduct().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.productName = product.getProductName();
            this.minQuantity = product.getMinQuantity();
            this.nowQuantity = product.getNowQuantity();
        }
        @Override
        public ProductResponse build() {
            return new ProductResponse(this);
        }
    }
}

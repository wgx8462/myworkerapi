package com.episode6.myworkerapi.model.product;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRequest {
    @NotNull
    @Schema(description = "상품 이름")
    private String productName;
    @NotNull
    @Schema(description = "기준이 될 최소 개수")
    private Short minQuantity;
    @NotNull
    @Schema(description = "현재 개수")
    private Short nowQuantity;
}

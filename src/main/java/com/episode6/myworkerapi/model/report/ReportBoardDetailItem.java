package com.episode6.myworkerapi.model.report;


import com.episode6.myworkerapi.entity.ReportBoard;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.format.DateTimeFormatter;

@Schema
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportBoardDetailItem {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "신고 유저 이름")
    private String reportUserName;
    @Schema(description = "신고 내용")
    private String reportContent;
    @Schema(description = "신고 날짜")
    private String dateReport;
    @Schema(description = "게시글 시퀀스")
    private Long boardId;
    @Schema(description = "게시글 작성자 이름")
    private String boardMemberUserName;
    @Schema(description = "게시글 작성 날짜")
    private String dateBoard;
    @Schema(description = "신고 게시글 내용")
    private String boardContent;

    private ReportBoardDetailItem(Builder builder) {
        this.id = builder.id;
        this.reportUserName = builder.reportUserName;
        this.reportContent = builder.reportContent;
        this.dateReport = builder.dateReport;
        this.boardId = builder.boardId;
        this.boardMemberUserName = builder.boardMemberUserName;
        this.dateBoard = builder.dateBoard;
        this.boardContent = builder.boardContent;
    }

    public static class Builder implements CommonModelBuilder<ReportBoardDetailItem> {

        private final Long id;
        private final String reportUserName;
        private final String reportContent;
        private final String dateReport;
        private final Long boardId;
        private final String boardMemberUserName;
        private final String dateBoard;
        private final String boardContent;

        public Builder(ReportBoard reportBoard) {
            this.id = reportBoard.getId();
            this.reportUserName = reportBoard.getMember().getName();
            this.reportContent = reportBoard.getContent();
            this.dateReport = reportBoard.getDateReport().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.boardId = reportBoard.getBoard().getId();
            this.boardMemberUserName = reportBoard.getBoard().getMember().getName();
            this.dateBoard = reportBoard.getBoard().getDateBoard().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.boardContent = reportBoard.getBoard().getContent();
        }

        @Override
        public ReportBoardDetailItem build() {
            return new ReportBoardDetailItem(this);
        }
    }
}

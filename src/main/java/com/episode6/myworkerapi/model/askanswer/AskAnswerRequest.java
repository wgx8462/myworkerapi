package com.episode6.myworkerapi.model.askanswer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "문의 답변 리퀘스트")
@Getter
@Setter
public class AskAnswerRequest {

    @Schema(description = "답변 제목", minLength = 2, maxLength = 20)
    private String answerTitle;

    @Schema(description = "답변 내용")
    private String answerContent;
}

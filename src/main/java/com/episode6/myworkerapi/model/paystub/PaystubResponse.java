package com.episode6.myworkerapi.model.paystub;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PaystubResponse {
    private Long totalPay;
    private LocalDate startDay;
    private LocalDate endDay;

    private PaystubResponse(Builder builder) {
        this.totalPay = builder.totalPay;
        this.startDay = builder.startDay;
        this.endDay = builder.endDay;
    }


    public static class Builder implements CommonModelBuilder<PaystubResponse> {
        private final Long totalPay;
        private final LocalDate startDay;
        private final LocalDate endDay;

        public Builder(LocalDate startDay, LocalDate endDay, Long totalPay) {
            this.totalPay = totalPay;
            this.startDay = startDay;
            this.endDay = endDay;
        }

        @Override
        public PaystubResponse build() {
            return new PaystubResponse(this);
        }
    }

}

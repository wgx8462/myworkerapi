package com.episode6.myworkerapi.model.business;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Schema(description = "사업장 등록")
public class BusinessRequest {
    @Schema(description = "상호명", maxLength = 30, minLength = 1)
    private String businessName;

    @Schema(description = "사업자 번호", maxLength = 12, minLength = 12)
    private String businessNumber;

    @Schema(description = "대표자명", maxLength = 20, minLength = 2)
    private String ownerName;

    @Schema(description = "사업자 등록증 이미지")
    private String businessImgUrl;

    @Schema(description = "업종")
    private String businessType;

    @Schema(description = "소재지", maxLength = 60, minLength = 10)
    private String businessLocation;

    @Schema(description = "이메일", maxLength = 50, minLength = 1)
    private String businessEmail;

    @Schema(description = "전화번호", maxLength = 13, minLength = 10)
    private String businessPhoneNumber;

    @Schema(description = "실 근무지 주소", maxLength = 60, minLength = 10)
    private String ReallyLocation;
}

package com.episode6.myworkerapi.model.business;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Request;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.request.RequestItem;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessResponse {

    private Long id;

    @Schema(description = "멤버Id")
    private Long memberId;

    @Schema(description = "상호명")
    private String businessName;

    @Schema(description = "사업자 번호")
    private String businessNumber;

    @Schema(description = "대표자명")
    private String ownerName;

    @Schema(description = "사업자등록증 이미지")
    private String businessImgUrl;

    @Schema(description = "업종")
    private String businessType;

    @Schema(description = "소재지")
    private String businessLocation;

    @Schema(description = "이메일")
    private String businessEmail;

    @Schema(description = "전화번호")
    private String businessPhoneNumber;

    @Schema(description = "실근무지 주소")
    private String ReallyLocation;

    @Schema(description = "실근무지위도")
    private Double latitudeBusiness;

    @Schema(description = "실근무지경도")
    private Double longitudeBusiness;

    @Schema(description = "사업장 영업여부")
    private String isActivity;

    @Schema(description = "등록 일시")
    private String dateJoinBusiness;

    @Schema(description = "승인 여부")
    private String isApprovalBusiness;

    @Schema(description = "가입 승인 일시")
    private String dateApprovalBusiness;

    @Schema(description = "사업장 등록 반려 사유")
    private String refuseReason;

    @Schema(description = "사업장 수정 요청 반려 사유")
    private String refuseFix;

    private RequestItem requestItem;

    private BusinessResponse(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.businessName = builder.businessName;
        this.businessNumber = builder.businessNumber;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this.businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.ReallyLocation = builder.ReallyLocation;
        this.latitudeBusiness = builder.latitudeBusiness;
        this.longitudeBusiness = builder.longitudeBusiness;
        this.isActivity = builder.isActivity;
        this.dateJoinBusiness = builder.dateJoinBusiness;
        this.isApprovalBusiness = builder.isApprovalBusiness;
        this.dateApprovalBusiness = builder.dateApprovalBusiness;
        this.refuseReason = builder.refuseReason;
        this.refuseFix = builder.refuseFix;

        this.requestItem = builder.requestItem;
    }

    public static class Builder implements CommonModelBuilder<BusinessResponse> {

        private final Long id;
        private final Long memberId;
        private final String businessName;
        private final String businessNumber;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessLocation;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final String ReallyLocation;
        private final Double latitudeBusiness;
        private final Double longitudeBusiness;
        private final String isActivity;
        private final String dateJoinBusiness;
        private final String isApprovalBusiness;
        private final String dateApprovalBusiness;
        private final String refuseReason;
        private final String refuseFix;

        private RequestItem requestItem;

        public Builder(Business business) {
            this.id = business.getId();
            this.memberId = business.getMember().getId();
            this.businessName = business.getBusinessName();
            this.businessNumber = business.getBusinessNumber();
            this.ownerName = business.getOwnerName();
            this.businessImgUrl = business.getBusinessImgUrl();
            this.businessType = business.getBusinessType();
            this.businessLocation = business.getBusinessLocation();
            this.businessEmail = business.getBusinessEmail();
            this.businessPhoneNumber = business.getBusinessPhoneNumber();
            this.ReallyLocation = business.getReallyLocation();
            this.latitudeBusiness = business.getLatitudeBusiness();
            this.longitudeBusiness = business.getLongitudeBusiness();
            this.isActivity = business.getIsActivity() ? "정상 영업중" : "폐업";
            this.dateJoinBusiness = business.getDateJoinBusiness().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.isApprovalBusiness = business.getIsApprovalBusiness() ? "승인 완료" : "승인 대기중";
            this.dateApprovalBusiness = business.getDateApprovalBusiness() == null ? null : business.getDateApprovalBusiness().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.refuseReason = business.getRefuseReason();
            this.refuseFix = business.getRefuseFix();
        }

        public Builder requestItem(Request request) {
            this.requestItem = request == null ? null : new RequestItem.Builder(request).build();
            return this;
        }
        @Override
        public BusinessResponse build() {
            return new BusinessResponse(this);
        }
    }
}

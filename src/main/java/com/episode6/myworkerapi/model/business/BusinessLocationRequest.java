package com.episode6.myworkerapi.model.business;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema
public class BusinessLocationRequest {
    @Schema(description = "실근무지 주소")
    private String reallyLocation;
}

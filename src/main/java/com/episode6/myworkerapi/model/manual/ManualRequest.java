package com.episode6.myworkerapi.model.manual;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


@Getter
@Schema
@Setter
public class ManualRequest {
    @Schema(description = "제목", maxLength = 30,minLength = 1)
    private String title;
    @Schema(description = "내용")
    private String content;
}

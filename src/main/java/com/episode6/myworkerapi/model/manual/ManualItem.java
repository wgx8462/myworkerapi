package com.episode6.myworkerapi.model.manual;


import com.episode6.myworkerapi.entity.Manual;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManualItem {

    private Long id;
    @Schema(description = "멤버 id")
    private Long memberId;
    @Schema(description = "멤버 이름")
    private String memberName;
    @Schema(description = "사업장 id")
    private Long businessId;
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "작성날짜")
    private String dateManual;
    @Schema(description = "게시물 제목")
    private String title;
    @Schema(description = "내용")
    private String content;


    private ManualItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.dateManual = builder.dateManual;
        this.title = builder.title;
        this.content = builder.content;
    }

    public static class Builder implements CommonModelBuilder<ManualItem> {

        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final Long businessId;
        private final String businessName;
        private final String dateManual;
        private final String title;
        private final String content;

        public Builder(Manual manual) {
            this.id = manual.getId();
            this.memberId = manual.getMember().getId();
            this.memberName = manual.getMember().getName();
            this.businessId = manual.getBusiness().getId();
            this.businessName = manual.getBusiness().getBusinessName();
            this.dateManual = manual.getDateManual().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
            this.title = manual.getTitle();
            this.content = manual.getContent();
        }

        @Override
        public ManualItem build() {
            return new ManualItem(this);
        }
    }
}

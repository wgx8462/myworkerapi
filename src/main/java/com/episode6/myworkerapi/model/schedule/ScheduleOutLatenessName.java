package com.episode6.myworkerapi.model.schedule;

import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ScheduleOutLatenessName {
    private String businessMemberName;
    private String startWork;

    private ScheduleOutLatenessName(Builder builder) {
        this.businessMemberName = builder.businessMemberName;
        this.startWork = builder.startWork;
    }

    public static class Builder implements CommonModelBuilder<ScheduleOutLatenessName> {
        private final String businessMemberName;
        private final String startWork;

        public Builder(Schedule schedule) {
            this.businessMemberName = schedule.getBusinessMember().getMember().getName();
            this.startWork = schedule.getStartWork().toString();
        }

        @Override
        public ScheduleOutLatenessName build() {
            return new ScheduleOutLatenessName(this);
        }
    }
}

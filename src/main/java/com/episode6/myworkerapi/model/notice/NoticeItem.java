package com.episode6.myworkerapi.model.notice;

import com.episode6.myworkerapi.entity.Notice;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoticeItem {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "관리자 시퀀스")
    private Long memberId;
    @Schema(description = "멤버 타입")
    private String memberType;
    @Schema(description = "공지 제목")
    private String title;
    @Schema(description = "공지 등록 날짜")
    private String dateNotice;

    private NoticeItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberType = builder.memberType;
        this.title = builder.title;
        this.dateNotice = builder.dateNotice;
    }

    public static class Builder implements CommonModelBuilder<NoticeItem> {
        private final Long id;
        private final Long memberId;
        private final String memberType;
        private final String title;
        private final String dateNotice;

        public Builder(Notice notice) {
            this.id = notice.getId();
            this.memberId = notice.getMember().getId();
            this.memberType = notice.getMember().getMemberType().getName();
            this.title = notice.getTitle();
            this.dateNotice = notice.getDateNotice().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }
        @Override
        public NoticeItem build() {
            return new NoticeItem(this);
        }
    }
}

package com.episode6.myworkerapi.model.request;


import com.episode6.myworkerapi.entity.RequestFix;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FixItem {
    private Long id;
    @Schema(description = "비즈니스 id")
    private Long BusinessId;
    @Schema(description = "상호명")
    private String businessName;
    @Schema(description = "대표자명")
    private String ownerName;
    @Schema(description = "전화번호")
    private String businessPhoneNumber;
    @Schema(description = "비고")
    private String ectMemo;

    private FixItem(Builder builder) {
        this.id = builder.id;
        this.BusinessId = builder.BusinessId;
        this.businessName = builder.businessName;
        this.ownerName = builder.ownerName;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.ectMemo = builder.ectMemo;
    }

    public static class Builder implements CommonModelBuilder<FixItem> {
        private final Long id;
        private final Long BusinessId;
        private final String businessName;
        private final String ownerName;
        private final String businessPhoneNumber;
        private final String ectMemo;

        public Builder(RequestFix fix) {
            this.id = fix.getId();
            this.BusinessId = fix.getBusiness().getId();
            this.businessName = fix.getBusinessName();
            this.ownerName = fix.getOwnerName();
            this.businessPhoneNumber = fix.getBusinessPhoneNumber();
            this.ectMemo = fix.getEtcMemo();
        }
        @Override
        public FixItem build() {
            return new FixItem(this);
        }
    }
}

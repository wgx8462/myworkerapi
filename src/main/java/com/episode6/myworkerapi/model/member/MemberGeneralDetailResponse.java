package com.episode6.myworkerapi.model.member;


import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberState;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberDetailItem;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberGeneralDetailResponse {
    @Schema(description = "멤버이름")
    private String MemberName;
    @Schema(description = "상태")
    private MemberState MemberState;
    @Schema(description = "생년월일")
    private LocalDate DateBirth;
    @Schema(description = "전화번호")
    private String PhoneNumber;
    @Schema(description = "아이디")
    private String UserName;
    @Schema(description = "가입날짜")
    private String DateMember;
    @Schema(description = "직원 리스트")
    private List<BusinessMemberDetailItem> BusinessMemberDetails;


    private MemberGeneralDetailResponse(Builder builder) {
        this.MemberName = builder.MemberName;
        this.MemberState = builder.MemberState;
        this.DateBirth = builder.DateBirth;
        this.PhoneNumber = builder.PhoneNumber;
        this.UserName = builder.UserName;
        this.DateMember = builder.DateMember;

        this.BusinessMemberDetails = builder.businessMemberDetailItems;

    }

    public static class Builder implements CommonModelBuilder<MemberGeneralDetailResponse> {

        private final String MemberName;

        private final MemberState MemberState;

        private final LocalDate DateBirth;

        private final String PhoneNumber;

        private final String UserName;

        private final String DateMember;

        private List<BusinessMemberDetailItem> businessMemberDetailItems;


        public Builder(Member member, List<BusinessMemberDetailItem> businessMemberDetailItems) {
            this.MemberName = member.getName();
            this.MemberState = member.getMemberState();
            this.DateBirth = member.getDateBirth();
            this.PhoneNumber = member.getPhoneNumber();
            this.UserName = member.getUsername();
            this.DateMember = member.getDateMember().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));

            this.businessMemberDetailItems = businessMemberDetailItems;

        }

        public Builder setBusiness(BusinessMember businessMember) {
            this.businessMemberDetailItems = businessMember == null ? null : businessMemberDetailItems;
            return this;
        }

        @Override
        public MemberGeneralDetailResponse build() {
            return new MemberGeneralDetailResponse(this);
        }
    }
}

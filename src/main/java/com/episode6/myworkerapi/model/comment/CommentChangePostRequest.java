package com.episode6.myworkerapi.model.comment;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema
public class CommentChangePostRequest {
    @Schema(description = "댓글 내용" ,minLength = 1)
    private String content;
}

package com.episode6.myworkerapi.model.board;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardChangePostRequest {
    @Schema(description = "제목", minLength = 1 ,maxLength = 30)
    private String title;
    @Schema(description = "본문", minLength = 1)
    private String content;
    @Schema(description = "이미지")
    private String boardImgUrl;
}

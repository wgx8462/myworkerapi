package com.episode6.myworkerapi.model.transition;

import com.episode6.myworkerapi.entity.Transition;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.format.DateTimeFormatter;

@Getter
@Setter
public class TransitionResponse {
    @Schema(description = "시퀀스")
    private Long id;
    @Schema(description = "비즈니스 시퀀스")
    private Long businessId;
    @Schema(description = "비즈니스 이름")
    private String businessName;
    @Schema(description = "회원 시퀀스")
    private Long memberId;
    @Schema(description = "회원 이름")
    private String memberName;
    @Schema(description = "인수인계 내용")
    private String content;
    @Schema(description = "인수인계 등록 날짜")
    private String dateTransition;
    @Schema(description = "인수인계 처리 여부")
    private String isFinish;
    @Schema(description = "인수인계 처리 완료 날짜")
    private String dateFinish;

    private TransitionResponse(Builder builder) {
        this.id = builder.id;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.content = builder.content;
        this.dateTransition = builder.dateTransition;
        this.isFinish = builder.isFinish;
        this.dateFinish = builder.dateFinish;
    }

    public static class Builder implements CommonModelBuilder<TransitionResponse> {
        private final Long id;
        private final Long businessId;
        private final String businessName;
        private final Long memberId;
        private final String memberName;
        private final String content;
        private final String dateTransition;
        private final String isFinish;
        private final String dateFinish;

        public Builder(Transition transition) {
            this.id = transition.getId();
            this.businessId = transition.getBusiness().getId();
            this.businessName = transition.getBusiness().getBusinessName();
            this.memberId = transition.getMember().getId();
            this.memberName = transition.getMember().getName();
            this.content = transition.getContent();
            this.dateTransition = transition.getDateTransition().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
            this.isFinish = transition.getIsFinish() ? "처리완료" : "미처리";
            this.dateFinish = transition.getDateFinish() == null ? null : transition.getDateFinish().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm"));
        }

        @Override
        public TransitionResponse build() {
            return new TransitionResponse(this);
        }
    }
}

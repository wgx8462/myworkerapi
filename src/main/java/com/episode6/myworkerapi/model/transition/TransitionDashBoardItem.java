package com.episode6.myworkerapi.model.transition;


import com.episode6.myworkerapi.entity.Transition;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Schema
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TransitionDashBoardItem {
    private Long id;
    @Schema(description = "인수인계 내용")
    private String transitionContent;
    @Schema(description = "처리여부")
    private Boolean isFinish;

    private TransitionDashBoardItem(Builder builder) {
        this.transitionContent = builder.transitionContent;
        this.isFinish = builder.isFinish;
        this.id = builder.id;
    }

    public static class Builder implements CommonModelBuilder<TransitionDashBoardItem> {
        private final String transitionContent;
        private final Boolean isFinish;
        private final Long id;


        public Builder(Transition transition) {
            this.transitionContent = transition.getContent();
            this.isFinish = transition.getIsFinish();
            this.id = transition.getId();
        }

        @Override
        public TransitionDashBoardItem build() {
            return new TransitionDashBoardItem(this);
        }
    }
}

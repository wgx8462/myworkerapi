package com.episode6.myworkerapi.model.dashboard;


import com.episode6.myworkerapi.entity.Notice;
import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.entity.Transition;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.notice.NoticeDashBoardItem;
import com.episode6.myworkerapi.model.product.ProductDashBoardItem;
import com.episode6.myworkerapi.model.transition.TransitionDashBoardItem;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DashboardResponse {
    private List<TransitionDashBoardItem> transitionDashBoardItems;
    private List<NoticeDashBoardItem> noticeDashBoardItems;
    private List<ProductDashBoardItem> productDashBoardItems;

    private DashboardResponse(Builder builder) {
        this.transitionDashBoardItems = builder.transitionDashBoardItems;
        this.noticeDashBoardItems = builder.noticeDashBoardItems;
        this.productDashBoardItems = builder.productDashBoardItems;
    }

    public static class Builder implements CommonModelBuilder<DashboardResponse> {
        private List<TransitionDashBoardItem> transitionDashBoardItems;
        private List<NoticeDashBoardItem> noticeDashBoardItems;
        private List<ProductDashBoardItem> productDashBoardItems;

        public Builder(List<TransitionDashBoardItem> transitionDashBoardItems, List<NoticeDashBoardItem> noticeDashBoardItems, List<ProductDashBoardItem> productDashBoardItems) {
            this.transitionDashBoardItems = transitionDashBoardItems;
            this.noticeDashBoardItems = noticeDashBoardItems;
            this.productDashBoardItems = productDashBoardItems;
        }

        public Builder setTransitionDashBoardItems(Transition transition) {
            this.transitionDashBoardItems = transition == null ? null : transitionDashBoardItems;
            return this;
        }

        public Builder seNoticeDashBoardItems(Notice notice) {
            this.noticeDashBoardItems = notice == null ? null : noticeDashBoardItems;
            return this;
        }

        public Builder setProductDashBoardItems(Product product) {
            this.productDashBoardItems = product == null ? null : productDashBoardItems;
            return this;
        }

        @Override
        public DashboardResponse build() {
            return new DashboardResponse(this);
        }
    }
}

package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BusinessMemberRepository extends JpaRepository<BusinessMember, Long> {
    Optional<BusinessMember> findByMemberAndBusinessAndIsWork(Member member, Business business, Boolean isWork);

    /**
     * 페이징 최신순
     */
    Page<BusinessMember> findAllByOrderByIdDesc(Pageable pageable);

    /**
     *
     * @param business 사업장 안에 알바생만 리스트로
     * @param pageable 페이징 최신순
     */
    Page<BusinessMember> findAllByBusinessOrderByIsWorkDescIdDesc(Business business, Pageable pageable);

    Page<BusinessMember> findAllByBusinessAndIsWorkOrderByIdDesc(Business business, Boolean isWork, Pageable pageable);

    BusinessMember findByBusinessAndMember(Business business, Member member);

    List<BusinessMember> findAllByMemberAndIsWork(Member member, Boolean isWork);

    List<BusinessMember> findByBusiness(Business business);

    List<BusinessMember> findByMember(Member member);

    List<BusinessMember> findByBusinessAndWeekScheduleContaining(Business business, String weekSchedule);


}

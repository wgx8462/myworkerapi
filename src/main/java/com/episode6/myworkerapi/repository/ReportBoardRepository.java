package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.ReportBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReportBoardRepository extends JpaRepository<ReportBoard, Long> {


    /**
     * 게시물 신고 최신순
     */
    Page<ReportBoard> findAllByOrderByIdDesc(Pageable pageable);

    /**
     * 리포트보드에서 보드아이디 찾기 (D)
     */
    List<ReportBoard> findByBoard_Id(long id);


    Optional<ReportBoard> findByMemberAndBoard(Member member, Board board);
}

package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Transition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TransitionRepository extends JpaRepository<Transition, Long> {
    Page<Transition> findAllByOrderByIdDesc(Pageable pageable);

    Page<Transition> findAllByBusinessOrderByIdDesc(Business business, Pageable pageable);

    List<Transition> findAllByBusinessAndDateTransitionBetween(Business business, LocalDateTime start, LocalDateTime end);
}

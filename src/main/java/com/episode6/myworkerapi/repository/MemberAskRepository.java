package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.MemberAsk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface MemberAskRepository extends JpaRepository<MemberAsk, Long> {
    Page<MemberAsk> findAllByOrderByIdDesc(Pageable pageable);
    Page<MemberAsk> findAllByMemberOrderByIdDesc(Member member, Pageable pageable);

    Long countByDateMemberAskBetween(LocalDateTime startDate, LocalDateTime endDate);
}

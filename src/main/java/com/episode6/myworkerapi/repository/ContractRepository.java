package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ContractRepository extends JpaRepository<Contract, Long> {

    Page<Contract> findAllByOrderByIdDesc(Pageable pageable);

    Contract findAllByBusinessMemberId(long id);

    Page<Contract> findByBusinessMember_Business(Pageable pageable, Business business);

    Page<Contract> findByBusinessMember(Pageable pageable, BusinessMember businessMember);

    Contract findByIdAndBusinessMember_Member(long id, Member member);

    Optional<Contract> findByBusinessMember_Member(Member member);

    Optional<Contract> findByBusinessMember(BusinessMember businessMember);

    Contract findByBusinessMemberOrderByIdAsc(BusinessMember businessMember);

}

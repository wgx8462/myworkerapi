package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.*;
import com.episode6.myworkerapi.model.dashboard.DashboardResponse;
import com.episode6.myworkerapi.model.notice.NoticeDashBoardItem;
import com.episode6.myworkerapi.model.product.ProductDashBoardItem;
import com.episode6.myworkerapi.model.transition.TransitionDashBoardItem;
import com.episode6.myworkerapi.repository.BusinessRepository;
import com.episode6.myworkerapi.repository.NoticeRepository;
import com.episode6.myworkerapi.repository.ProductRepository;
import com.episode6.myworkerapi.repository.TransitionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DashBoardTwoService {
    private final BusinessRepository businessRepository;
    private final TransitionRepository transitionRepository;
    private final NoticeRepository noticeRepository;
    private final ProductRepository productRepository;


    /**
     * 대시보드
     */
    public DashboardResponse getDashboard(Member member) {
        Business business = businessRepository.findBusinessByMember(member);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startTime = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0, 0);
        LocalDateTime endTime = startTime.plusDays(1).minusSeconds(1);


        List<Transition> transition = transitionRepository.findAllByBusinessAndDateTransitionBetween(business,startTime,endTime);
        List<Notice> notices = noticeRepository.findTop5ByIdGreaterThanEqualOrderByIdDesc(1);
        List<Product> products = productRepository.findByBusiness(business);

        List<TransitionDashBoardItem> transitionDashBoardItemList = new LinkedList<>();
        for (Transition transition1 : transition) transitionDashBoardItemList.add(new TransitionDashBoardItem.Builder(transition1).build());

        List<NoticeDashBoardItem> noticeDashBoardItemList = new LinkedList<>();
        for (Notice notice : notices) noticeDashBoardItemList.add(new NoticeDashBoardItem.Builder(notice).build());

        List<ProductDashBoardItem> productDashBoardItemList = new LinkedList<>();
            for (Product product : products){
                if (product.getMinQuantity() >= product.getNowQuantity()) productDashBoardItemList.add(new ProductDashBoardItem.Builder(product).build());
            }
        return new DashboardResponse.Builder(transitionDashBoardItemList,noticeDashBoardItemList,productDashBoardItemList).build();
    }
}

package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.AskAnswer;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.MemberAsk;
import com.episode6.myworkerapi.model.askanswer.AskAnswerItem;
import com.episode6.myworkerapi.model.askanswer.AskAnswerRequest;
import com.episode6.myworkerapi.model.askanswer.AskAnswerResponse;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.AskAnswerRepository;
import com.episode6.myworkerapi.repository.MemberAskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AskAnswerService {
    private final MemberAskRepository memberAskRepository;
    private final AskAnswerRepository askAnswerRepository;
    private final GCPService gcpService;

    /**
     *
     * @param member 답변하는 관리자
     * @param memberAsk 문의 내역
     * @param request 답변 내용
     */
    public void setAskAnswer(Member member, MemberAsk memberAsk, AskAnswerRequest request, MultipartFile multipartFile) throws IOException {
        Optional<AskAnswer> askAnswer = askAnswerRepository.findByMemberAskOrderById(memberAsk);
        if (askAnswer.isEmpty()) {
            if (multipartFile != null) {
                String imgUrl = gcpService.uploadObject(multipartFile);
            memberAsk.putIsAnswer();
            memberAskRepository.save(memberAsk);
            askAnswerRepository.save(new AskAnswer.Builder(member, memberAsk, request ,imgUrl).build());
            } else {
                memberAsk.putIsAnswer();
                memberAskRepository.save(memberAsk);
                askAnswerRepository.save(new AskAnswer.Builder(member, memberAsk, request).build());
            }
        }
    }

    /**
     * 문의 답변 내역 리스트
     * @param pageNum 현재 페이지
     * @return 페이징된 리스트
     */
    public ListResult<AskAnswerItem> getAskAnswers(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<AskAnswer> askAnswers = askAnswerRepository.findAllByOrderByIdDesc(pageRequest);

        List<AskAnswerItem> result = new LinkedList<>();
        for (AskAnswer askAnswer : askAnswers) result.add(new AskAnswerItem.Builder(askAnswer).build());

        return ListConvertService.settingResult(result, askAnswers.getTotalElements(), askAnswers.getTotalPages(), askAnswers.getPageable().getPageNumber());
    }

    /**
     * 문의 답변 상세보기
     * @param id 답변 id
     * @return 답변 내용
     */
    public AskAnswerResponse getAskAnswer(long id) {
        AskAnswer askAnswer = askAnswerRepository.findById(id).orElseThrow();
        return new AskAnswerResponse.Builder(askAnswer).build();
    }

    public AskAnswerResponse getAskAnswer(MemberAsk memberAsk) {
        AskAnswer askAnswer = askAnswerRepository.findByMemberAskOrderById(memberAsk).orElseThrow();
        return new AskAnswerResponse.Builder(askAnswer).build();
    }

    public AskAnswerResponse getAskAnswerMemberAsk(long id) {
        MemberAsk memberAsk = memberAskRepository.findById(id).orElseThrow();
        AskAnswer askAnswer = askAnswerRepository.findByMemberAsk(memberAsk);
        return new AskAnswerResponse.Builder(askAnswer).build();
    }

    public void putAskAnswer(long id, AskAnswerRequest request) {
        AskAnswer askAnswer = askAnswerRepository.findById(id).orElseThrow();
        askAnswer.putAskAnswer(request);
        askAnswerRepository.save(askAnswer);
    }
}

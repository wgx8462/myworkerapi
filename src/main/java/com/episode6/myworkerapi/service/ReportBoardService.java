package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.ReportBoard;
import com.episode6.myworkerapi.exception.CReportBoardOverlapException;
import com.episode6.myworkerapi.model.report.ReportBoardDetailItem;
import com.episode6.myworkerapi.model.report.ReportBoardItem;
import com.episode6.myworkerapi.model.report.ReportRequest;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.ReportBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReportBoardService {
    private final ReportBoardRepository reportBoardRepository;
    /**
     * 게시물 신고 등록 중복 불가능
     */
    public void setReportBoard(Member member, Board board, ReportRequest request) throws Exception {
        Optional<ReportBoard> optionalReportBoard = reportBoardRepository.findByMemberAndBoard(member, board);
        if (optionalReportBoard.isEmpty()) reportBoardRepository.save(new ReportBoard.Builder(request,board,member).build());
        else throw new CReportBoardOverlapException();
    }
    /**
     * 게시물 신고 최신순 페이징
     */
    public ListResult<ReportBoardItem> setReportBoardPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<ReportBoard> reportBoardPage = reportBoardRepository.findAllByOrderByIdDesc(pageRequest);

        List<ReportBoardItem> reportItemList = new LinkedList<>();

        for (ReportBoard reportBoard : reportBoardPage.getContent()) {
            reportItemList.add(new ReportBoardItem.Builder(reportBoard).build());
        }
        return ListConvertService.settingResult(
                reportItemList
                ,reportBoardPage.getTotalElements()
                ,reportBoardPage.getTotalPages()
                ,reportBoardPage.getPageable().getPageNumber()
        );
    }
    /**
     * 게시글 신고 삭제
     */
    public void delReportBoard(long id) {
        reportBoardRepository.deleteById(id);
    }

    /**
     * 신고내용과 신고 게시물 1대1로 보기
     */
    public ReportBoardDetailItem getReportBoardDetail(long Id) {
        ReportBoard reportBoard = reportBoardRepository.findById(Id).orElseThrow();
        return new ReportBoardDetailItem.Builder(reportBoard).build();
    }
}

package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberStartDayItem;
import com.episode6.myworkerapi.model.nearDayStatics.NearDayStaticsResponse;
import com.episode6.myworkerapi.model.schedule.ScheduleLatenessName;
import com.episode6.myworkerapi.model.schedule.ScheduleOutLatenessName;
import com.episode6.myworkerapi.model.statistics.StatisticsScheduleResponse;
import com.episode6.myworkerapi.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StatisticsService {
    private final MemberRepository memberRepository;
    private final BusinessRepository businessRepository;
    private final BoardRepository boardRepository;
    private final MemberAskRepository memberAskRepository;
    private final BusinessMemberRepository businessMemberRepository;
    private final ScheduleRepository scheduleRepository;

    /**
     * 하루치 차트
     */
    public NearDayStaticsResponse getNearStatistics() {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime startTime = LocalDateTime.of(
                today.getYear(),
                today.getMonth(),
                today.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime endTime = startTime.plusDays(1).minusSeconds(1);

        List<Long> joinDatasets = new LinkedList<>();
        List<Long> businessDatasets = new LinkedList<>();
        List<Long> newWriteDatasets = new LinkedList<>();
        List<Long> newInquiryDatasets = new LinkedList<>();

        NearDayStaticsResponse response = new NearDayStaticsResponse();


        joinDatasets.add(memberRepository.countByDateMemberBetween(startTime, endTime));
        businessDatasets.add(businessRepository.countByDateJoinBusinessBetween(startTime, endTime));
        newWriteDatasets.add(boardRepository.countByDateBoardBetween(startTime, endTime));
        newInquiryDatasets.add(memberAskRepository.countByDateMemberAskBetween(startTime, endTime));

        response.setJoinDatasets(joinDatasets);
        response.setBusinessDatasets(businessDatasets);
        response.setNewWriteDatasets(newWriteDatasets);
        response.setNewInquiryDatasets(newInquiryDatasets);

        return response;
    }

    /**
     * 한달치 차트
     */
    public NearDayStaticsResponse getMonthStatistics() {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime startTime = LocalDateTime.of(
                today.getYear(),
                today.getMonth(),
                today.getDayOfMonth(),
                23,
                59,
                59
        );
        LocalDateTime endTime = startTime.minusMonths(1).minusDays(1);

        List<Long> joinDatasets = new LinkedList<>();
        List<Long> businessDatasets = new LinkedList<>();
        List<Long> newWriteDatasets = new LinkedList<>();
        List<Long> newInquiryDatasets = new LinkedList<>();

        NearDayStaticsResponse response = new NearDayStaticsResponse();


        joinDatasets.add(memberRepository.countByDateMemberBetween(endTime, startTime));
        businessDatasets.add(businessRepository.countByDateJoinBusinessBetween(endTime, startTime));
        newWriteDatasets.add(boardRepository.countByDateBoardBetween(endTime, startTime));
        newInquiryDatasets.add(memberAskRepository.countByDateMemberAskBetween(endTime, startTime));

        response.setJoinDatasets(joinDatasets);
        response.setBusinessDatasets(businessDatasets);
        response.setNewWriteDatasets(newWriteDatasets);
        response.setNewInquiryDatasets(newInquiryDatasets);

        return response;
    }

    public StatisticsScheduleResponse getDashBoard(Business business) {
        int toWeek = LocalDate.now().getDayOfWeek().getValue();
        String[] weekText = new String[] {"월", "화", "수", "목", "금", "토", "일"};
        String todayText = weekText[toWeek - 1];
        List<BusinessMember> businessMembers = businessMemberRepository.findByBusinessAndWeekScheduleContaining(business, todayText);

        List<BusinessMemberStartDayItem> businessMemberStartDayItems = new LinkedList<>();
        for (BusinessMember businessMember : businessMembers) {
            businessMemberStartDayItems.add(new BusinessMemberStartDayItem.Builder(businessMember, todayText).build());
        }

        List<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessAndDateWork(business, LocalDate.now());
        List<ScheduleLatenessName> scheduleLatenessNames = new LinkedList<>();
        for (Schedule schedule : schedules) {
            scheduleLatenessNames.add(new ScheduleLatenessName.Builder(schedule).build());
        }

        List<Schedule> outSchedules = scheduleRepository.findAllByBusinessMember_BusinessAndDateWorkAndIsLateness(business, LocalDate.now(), true);
        List<ScheduleOutLatenessName> scheduleOutLatenessNames = new LinkedList<>();
        for (Schedule schedule : outSchedules) {
            scheduleOutLatenessNames.add(new ScheduleOutLatenessName.Builder(schedule).build());
        }


        return new StatisticsScheduleResponse.Builder(businessMemberStartDayItems, scheduleLatenessNames, scheduleOutLatenessNames).build();
    }
}

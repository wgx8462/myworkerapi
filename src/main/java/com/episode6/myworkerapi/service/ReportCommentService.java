package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.*;
import com.episode6.myworkerapi.exception.CReportCommentOverlapException;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.report.ReportCommentDetailItem;
import com.episode6.myworkerapi.model.report.ReportCommentItem;
import com.episode6.myworkerapi.model.report.ReportRequest;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.repository.ReportCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ReportCommentService {
    private final ReportCommentRepository reportCommentRepository;

    /**
     * 댓글 신고 등록 중복 불가능
     */
    public void setReportComment(Member member, Comment comment, ReportRequest request) throws Exception{
        Optional<ReportComment> optionalReportComment = reportCommentRepository.findByMemberAndComment(member, comment);
        CommonResult result = new CommonResult();

        if (optionalReportComment.isEmpty()) reportCommentRepository.save(new ReportComment.Builder(request,member,comment).build());
        else throw new CReportCommentOverlapException();
    }

    /**
     * 댓글 신고 최신순 페이징
     */
    public ListResult<ReportCommentItem> setReportCommentPage(int pageNum) {

        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<ReportComment> reportcommentPage = reportCommentRepository.findAllByOrderByIdDesc(pageRequest);

        List<ReportCommentItem> reportItemList = new LinkedList<>();
        for (ReportComment reportComment : reportcommentPage.getContent()) {
            reportItemList.add(new ReportCommentItem.Builder(reportComment).build());
        }
        return ListConvertService.settingResult(
                reportItemList
                ,reportcommentPage.getTotalElements()
                ,reportcommentPage.getTotalPages()
                ,reportcommentPage.getPageable().getPageNumber()
        );
    }

    /**
     * 댓글 신고 삭제
     */
    public void delReportComment(long id) {
        reportCommentRepository.deleteById(id);
    }

    /**
     * 신고 용과 신고 댓글 일대일로 보기
     */
    public ReportCommentDetailItem getReportCommentDetail(long id) {
        ReportComment reportComment = reportCommentRepository.findById(id).orElseThrow();
        return new ReportCommentDetailItem.Builder(reportComment).build();
    }
}

package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.contract.ContractRequest;
import com.episode6.myworkerapi.model.contract.ContractResponse;
import com.episode6.myworkerapi.repository.BusinessMemberRepository;
import com.episode6.myworkerapi.repository.ContractRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractService {
    private final ContractRepository contractRepository;
    private final BusinessMemberRepository businessMemberRepository;

    public Contract getContractData(long id) {
        return contractRepository.findById(id).orElseThrow();
    }

    /**
     * 계약 등록
     */
    public void setContract(BusinessMember businessMember, ContractRequest request) {
        contractRepository.save(new Contract.Builder(request,businessMember).build());
    }

    /**
     * 계약 상세보기
     */
    public ContractResponse getContract(long id) {
        Contract contract = contractRepository.findById(id).orElseThrow();
        return new ContractResponse.Builder(contract).build();
    }

    /**
     * 멤버 id 에 해당하는 계약서 페이징
     */
    public ListResult<ContractResponse> getWorkerContract(int pageNum, Business business, Member member) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        BusinessMember businessMember = businessMemberRepository.findByBusinessAndMember(business, member);
        Page<Contract> contractPage = contractRepository.findByBusinessMember(pageRequest, businessMember);
        List<ContractResponse> contractResponses = new LinkedList<>();
        for (Contract contract : contractPage.getContent()) contractResponses.add(new ContractResponse.Builder(contract).build());

        return ListConvertService.settingResult(
                contractResponses
                ,contractPage.getTotalElements()
                ,contractPage.getTotalPages()
                ,contractPage.getPageable().getPageNumber()
        );
    }

    /**
     * 계약 페이징 최신순 (관리자)
     */
    public ListResult<ContractResponse> getContracts(int pageNum){
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<Contract> responsePage = contractRepository.findAllByOrderByIdDesc(pageRequest);

        List<ContractResponse> responseList = new LinkedList<>();
        for (Contract contract : responsePage.getContent()) responseList.add(new ContractResponse.Builder(contract).build());

        return ListConvertService.settingResult(
                responseList
                ,responsePage.getTotalElements()
                ,responsePage.getTotalPages()
                ,responsePage.getPageable().getPageNumber()
        );
    }

    /**
     * 사업장 id , 계약 페이징
     */
    public ListResult<ContractResponse> getOwnerContractPage(int pageNum, Business business) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 6);
        Page<Contract> responsePage = contractRepository.findByBusinessMember_Business(pageRequest,business);

        List<ContractResponse> responseList = new LinkedList<>();
        for (Contract contract : responsePage.getContent()) responseList.add(new ContractResponse.Builder(contract).build());

        return ListConvertService.settingResult(
                responseList
                ,responsePage.getTotalElements()
                ,responsePage.getTotalPages()
                ,responsePage.getPageable().getPageNumber()
        );
    }
}

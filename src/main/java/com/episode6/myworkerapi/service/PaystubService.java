package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.model.paystub.PaystubResponse;
import com.episode6.myworkerapi.repository.ContractRepository;
import com.episode6.myworkerapi.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PaystubService {
    private final ScheduleRepository scheduleRepository;
    private final ContractRepository contractRepository;


//    public void setPaystub(PaystubRequest request, Contract contract) {
//        Schedule schedule = scheduleRepository.findByBusinessMember(contract.getBusinessMember());
//        Double plusWorkTime = schedule.getPlusWorkTime();
//
//        if (plusWorkTime > 0) {
//            request.setWeekRestPay(plusWorkTime * (contract.getPayPrice() * 1.5));
//        } else  request.setWeekRestPay(0D);
//
//        if (contract.getPayType() == PayType.TIME_WAGE) {
//            List<Schedule> schedules = scheduleRepository.findAllByDateWorkBetween(request.getDateStartWork() , request.getDateEndWork());
//            for (Schedule schedule1 : schedules) {
//                request.setPayBasic( + schedule1.getTotalWorkTime());
//            }
//        } else request.setPayBasic(0D);
//
//        paystubRepository.save(new Paystub.Builder(request,contract).build());
//    }

    /**
     *  알바생 시급으로 한달 월급 계산하기
     */
    public PaystubResponse getMonthlyPay(Member member, int year, int month) {
        Contract contract = contractRepository.findByBusinessMember_Member(member).orElseThrow();
        double timePerPay = contract.getPayPrice();

        LocalDate dateStart = LocalDate.of(year, month, 1);
        LocalDate dateEnd = dateStart.plusMonths(1).minusDays(1);

        List<Schedule> scheduleList = scheduleRepository.findAllByDateWorkBetween(dateStart, dateEnd);
        long totalPay = scheduleList.stream().mapToLong(schedule -> getDayPay(timePerPay, schedule.getTotalWorkTime())).sum(); // 이거 총 금액임

        return new PaystubResponse.Builder(dateStart, dateEnd, totalPay).build();
    }

    private long getDayPay(double timePerPay, int totalWorkTime) {
        int fixTime = 8;
        double basePay = totalWorkTime <= fixTime ? totalWorkTime * timePerPay :  fixTime * timePerPay;
        double addPay = totalWorkTime > fixTime ? (totalWorkTime - fixTime) * (timePerPay * 1.5) : 0D;
        return Math.round(basePay + addPay);
    }
}

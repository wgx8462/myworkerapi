package com.episode6.myworkerapi.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
@RequiredArgsConstructor
public class GCPService {
    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    @Value("${spring.cloud.key}")
    private String SECRET_KEY;

    public String uploadObject(MultipartFile multipartFile) throws IOException {
        InputStream keyFile = ResourceUtils.getURL("classpath:" + SECRET_KEY).openStream();

        Storage storage;
        storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, multipartFile.getOriginalFilename())
                .setContentType(multipartFile.getContentType()).build();

        Blob blob = storage.create(blobInfo, multipartFile.getInputStream());

        return "https://storage.googleapis.com/" + bucketName + "/" + multipartFile.getOriginalFilename();
    }
}

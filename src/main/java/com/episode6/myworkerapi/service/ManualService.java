package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Manual;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.manual.ManualItem;
import com.episode6.myworkerapi.model.manual.ManualRequest;
import com.episode6.myworkerapi.model.manual.ManualResponse;
import com.episode6.myworkerapi.repository.ManualRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ManualService {
    private final ManualRepository manualRepository;
    private final GCPService gcpService;

    /**
     * 메뉴얼 등록 , gcp 사진등록
     */
    public void setManual(ManualRequest request, Member member, Business business, MultipartFile multipartFile) throws IOException {
        if (multipartFile != null) {
            String imgUrl = gcpService.uploadObject(multipartFile);
            manualRepository.save(new Manual.Builder(request, member, business,imgUrl).build());
        } else  manualRepository.save(new Manual.Builder(request, member, business).build());

    }

    /**
     * 메뉴얼 상세보기
     */
    public ManualResponse getManual(long id) {
        Manual manual = manualRepository.findById(id).orElseThrow();
        return new ManualResponse.Builder(manual).build();
    }

    /**
     * 메뉴얼 페이징 최신순 (사업장)
     */
    public ListResult<ManualItem> getManuals(int pageNum, Business business) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Manual> manualPage = manualRepository.findByBusinessOrderByIdDesc(pageRequest, business);

        List<ManualItem> manualItems = new LinkedList<>();
        for (Manual manual : manualPage.getContent()) manualItems.add(new ManualItem.Builder(manual).build());

        return ListConvertService.settingResult(
                manualItems
                ,manualPage.getTotalElements()
                ,manualPage.getTotalPages()
                ,manualPage.getPageable().getPageNumber());
    }

    /**
     * 메뉴얼 페이징 최신순 (관리자)
     */
    public ListResult<ManualItem> getAllManuals(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Manual> manualPage = manualRepository.findAllByOrderByIdDesc(pageRequest);

        List<ManualItem> manualItems = new LinkedList<>();
        for (Manual manual : manualPage.getContent()) manualItems.add(new ManualItem.Builder(manual).build());

        return ListConvertService.settingResult(
                manualItems
                ,manualPage.getTotalElements()
                ,manualPage.getTotalPages()
                ,manualPage.getPageable().getPageNumber()
        );
    }
}



package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.product.ProductChangeRequest;
import com.episode6.myworkerapi.model.product.ProductRequest;
import com.episode6.myworkerapi.model.productrecord.ProductRecordRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessId", nullable = false)
    private Business business;

    @Column(nullable = false)
    private LocalDateTime dateProduct;

    @Column(nullable = false, length = 25)
    private String productName;

    @Column(nullable = false)
    private Short minQuantity;

    @Column(nullable = false)
    private Short nowQuantity;

    public void putProduct(ProductChangeRequest request) {
        this.productName = request.getProductName();
        this.minQuantity = request.getMinQuantity();
    }
    public void putProduct(Member member, ProductRecordRequest request) {
        this.member = member;
        this.nowQuantity = request.getNowQuantity();
        this.dateProduct = LocalDateTime.now();
    }

    private Product(Builder builder) {
        this.member = builder.member;
        this.business = builder.business;
        this.dateProduct = builder.dateProduct;
        this.productName = builder.productName;
        this.minQuantity = builder.minQuantity;
        this.nowQuantity = builder.nowQuantity;
    }

    public static class Builder implements CommonModelBuilder<Product> {
        private final Member member;
        private final Business business;
        private final LocalDateTime dateProduct;
        private final String productName;
        private final Short minQuantity;
        private final Short nowQuantity;

        public Builder(Business business, Member member, ProductRequest request) {
            this.member = member;
            this.business = business;
            this.dateProduct = LocalDateTime.now();
            this.productName = request.getProductName();
            this.minQuantity = request.getMinQuantity();
            this.nowQuantity = request.getNowQuantity();
        }

        @Override
        public Product build() {
            return new Product(this);
        }
    }
}

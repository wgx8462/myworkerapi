package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.enums.Position;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberRequest;
import com.episode6.myworkerapi.model.businessmember.BusinessMemberScheduleRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMember {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessId", nullable = false)
    private Business business;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Position position;

    @Column(nullable = false)
    private Boolean isWork;

    @Column(nullable = false)
    private LocalDate dateIn;

    private LocalDate dateOut;

    @Column(columnDefinition = "TEXT")
    private String etc;

    @Column(length = 40)
    private String weekSchedule;

    @Column(length = 60)
    private String timeScheduleStart;

    @Column(length = 60)
    private String timeScheduleEnd;

    @Column(length = 30)
    private String timeScheduleTotal;

    @Column(length = 20)
    private String timeScheduleRest;

    public void putMemberSchedule(BusinessMemberScheduleRequest request) {
        this.weekSchedule = request.getWeekSchedule();
        this.timeScheduleStart = request.getTimeScheduleStart();
        this.timeScheduleEnd = request.getTimeScheduleEnd();
        this.timeScheduleTotal = request.getTimeScheduleTotal();
    }

    public void putChangeIsWork() {
        this.isWork = false;
        this.dateOut = LocalDate.now();
    }

    private BusinessMember(Builder builder) {
        this.member = builder.member;
        this.business = builder.business;
        this.position = builder.position;
        this.isWork = builder.isWork;
        this.dateIn = builder.dateIn;
        this.dateOut = builder.dateOut;
        this.etc = builder.etc;
        this.weekSchedule = builder.weekSchedule;
        this.timeScheduleStart = builder.timeScheduleStart;
        this.timeScheduleEnd = builder.timeScheduleEnd;
        this.timeScheduleTotal = builder.timeScheduleTotal;
        this.timeScheduleRest = builder.timeScheduleRest;
    }

    public static class Builder implements CommonModelBuilder<BusinessMember> {
        private final Member member;
        private final Business business;
        private final Position position;
        private final Boolean isWork;
        private final LocalDate dateIn;
        private final LocalDate dateOut;
        private final String etc;
        private final String weekSchedule;
        private final String timeScheduleStart;
        private final String timeScheduleEnd;
        private final String timeScheduleTotal;
        private final String timeScheduleRest;

        public Builder(Member member, Business business, BusinessMemberRequest request) {
            this.member = member;
            this.business = business;
            this.position = request.getPosition();
            this.isWork = true;
            this.dateIn = LocalDate.now();
            this.dateOut = null;
            this.etc = request.getEtc();
            this.weekSchedule = null;
            this.timeScheduleStart = null;
            this.timeScheduleEnd = null;
            this.timeScheduleTotal = null;
            this.timeScheduleRest = null;
        }
        @Override
        public BusinessMember build() {
            return new BusinessMember(this);
        }
    }
}

package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.transition.TransitionRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Transition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessId", nullable = false)
    private Business business;

    @Column(nullable = false)
    private LocalDateTime dateTransition;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    @Column(nullable = false)
    private Boolean isFinish;

    private LocalDateTime dateFinish;

    public void putContent(TransitionRequest request) {
        this.content = request.getContent();
    }

    public void putFinish() {
        this.isFinish = true;
        this.dateFinish = LocalDateTime.now();
    }

    private Transition(Builder builder) {
        this.member = builder.member;
        this.business = builder.business;
        this.dateTransition = builder.dateTransition;
        this.content = builder.content;
        this.isFinish = builder.isFinish;
        this.dateFinish = builder.dateFinish;
    }

    public static class Builder implements CommonModelBuilder<Transition> {
        private final Member member;
        private final Business business;
        private final LocalDateTime dateTransition;
        private final String content;
        private final Boolean isFinish;
        private final LocalDateTime dateFinish;

        public Builder(Business business, Member member, TransitionRequest request) {
            this.member = member;
            this.business = business;
            this.dateTransition = LocalDateTime.now();
            this.content = request.getContent();
            this.isFinish = false;
            this.dateFinish = null;
        }
        @Override
        public Transition build() {
            return new Transition(this);
        }
    }
}

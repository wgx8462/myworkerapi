package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.report.ReportRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "commentId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Comment comment;

    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    private String content;

    @Column(nullable = false)
    private LocalDateTime dateReport;


    private ReportComment(Builder builder) {
        this.comment = builder.comment;
        this.member = builder.member;
        this.content = builder.content;
        this.dateReport = builder.dateReport;
    }

    public static class Builder implements CommonModelBuilder<ReportComment> {
        private final Comment comment;
        private final Member member;
        private final String content;
        private final LocalDateTime dateReport;


        public Builder(ReportRequest request, Member member, Comment comment) {
            this.comment = comment;
            this.member = member;
            this.content = request.getContent();
            this.dateReport = LocalDateTime.now();
        }
        @Override
        public ReportComment build() {
            return new ReportComment(this);
        }
    }
}

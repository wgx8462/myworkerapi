package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.request.RefuseReasonRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessId", nullable = false)
    private Business business;

    @Column(nullable = false)
    private LocalDateTime dateRequest;


    private Request(Builder builder) {
        this.business = builder.business;
        this.dateRequest = builder.dateRequest;
    }



    public static class Builder implements CommonModelBuilder<Request> {
        private final Business business;
        private final LocalDateTime dateRequest;


        public Builder(Business business) {
            this.business = business;
            this.dateRequest = LocalDateTime.now();
        }
        @Override
        public Request build() {
            return new Request(this);
        }
    }
}

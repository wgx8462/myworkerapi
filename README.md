# 내 알바야 프로젝트
### 팀명: episode6 (6인)
### 팀원 소개
<img src="images/kb.png" width= 150 alt="내사진"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<img src="images/yj.png" width= 150 alt="내사진"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/me02.jpg" width= 150 alt="내사진"/> <br>

#### 백엔드 : 김기범 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;프론트 : 이윤진 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 프론트 : 홍수경
<br>
<img src="images/jo.jpg" width= 150 alt="내사진"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<img src="images/jo.png" width= 150 alt="내사진"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/go.png" width= 150 alt="내사진"/> <br>

#### 백엔드 : 최재오 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;프론트 : 최정연 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 프론트 : 노지은
<br>



### 프로젝트 소개 
* 이 프로젝트는 혼자서 알바생을 관리하기 힘든 사장님들을 위한 프로젝트입니다.
* 사장님의 매장의 인수인계 사항이나, 메뉴얼 등을 쉽게 전달할 수 있습니다.
* 알바생 앱에서는 위도, 경도에 따른 위치기반으로 출퇴근을 체크합니다.
* 근로계약서를 앱에 저장하고 관리할 수 있습니다.
### 개발기간 : 2024 - 03.06 ~ 05.08
***
### 사용한 기술
* **Spring boot, Docker, PostgreSQL, Google Cloud Platform(GCP)**
### 프로젝트 아키텍처
![사용한 기술](./images/ProjectArchitecture.png)
### 프로젝트 주요 기능 

* 알바생 출퇴근 gps로 기록 
* 기록을 기반으로한 급여관리
* 출퇴근 한달 스케쥴 한눈에 보기

### 프론트 깃 주소
#### **사장님 웹:** [이동하기](https://gitlab.com/lee_rado/my-worker-web2)

#### **사장님 앱:** [이동하기](https://gitlab.com/my-worker1/my_worker_app)

#### **관리자 웹:** [이동하기](https://gitlab.com/jy20141013/myworker)

#### **알바생 앱:** [이동하기](https://gitlab.com/jieun08316/my-worker-app)